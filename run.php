<?php

require __DIR__ . '\bootstrap.php';
$config = include __DIR__ . '\config\main.php';

use components\cli\CliArg;
use components\db\PDOConfigurator;
use components\game\MatchMaker;
use models\User;

$pdo = PDOConfigurator::configure($config['db']);

$uid = CliArg::create(1)
	->setRequired(true)
	->setFilter('\intval')
	->get();
$user = User::findOne($uid, $pdo);
if ($user === null) {
	exit('User not found');
}

$filter = CliArg::create(2)
	->setDefault([])
	->setFilter(function ($value) {
		return array_map(
			'\intval',
			explode(',', $value)
		);
	})->get();

$result = MatchMaker::create($pdo)
	->setUser($user)
	->setFilter($filter)
	->run();

$result = array_map(function (User $user) {
	return $user->id;
}, $result);
echo implode(',', $result);
