<?php

foreach (['main', 'test'] as $name)
{
	$fileName = __DIR__ . sprintf('\config\%s-local.php', $name);
	if (!file_exists($fileName)) {
		file_put_contents($fileName, '<?php return [];');
	}
}
