DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id SERIAL PRIMARY KEY NOT NULL,
    username CHARACTER VARYING (255) NOT NULL,
    authority INTEGER NOT NULL
);

CREATE UNIQUE INDEX users_username_idx ON users USING btree(username);

CREATE INDEX users_authority_idx ON users USING btree(authority);
