<?php spl_autoload_register(function($className) {
	$fileName = __DIR__ . DIRECTORY_SEPARATOR
		. 'src' . DIRECTORY_SEPARATOR
		. str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
	if (!file_exists($fileName) || is_dir($fileName)) {
		return;
	}
	require $fileName;
});
