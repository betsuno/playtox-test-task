<?php

use components\db\PDOConfigurator;
use components\exceptions\InvalidCallException;
use components\game\MatchMaker;
use models\User;
use PHPUnit\Framework\TestCase;

class MatchMakerTest extends TestCase
{
	private static PDO $dbh;

	public static function setUpBeforeClass(): void
	{
		$config = include '..\config\test.php';
		self::$dbh = PDOConfigurator::configure($config['db']);
		self::$dbh->query(file_get_contents('..\sql\structure.sql'));
		self::$dbh->query(file_get_contents('..\sql\fixture.sql'));
	}

	public function testUserNotProvided()
	{
		$this->expectException(InvalidCallException::class);
		MatchMaker::create(self::$dbh)->run();
	}

	public function testBadFilters()
	{
		$this->expectException(InvalidCallException::class);
		$user = User::findOne(1, self::$dbh);
		MatchMaker::create(self::$dbh)
			->setUser($user)
			->setFilter(['foo', 'bar'])
			->run();
	}

	public function testInTheMiddleOfList()
	{
		$user = User::findOne(50, self::$dbh);
		$result = MatchMaker::create(self::$dbh)
			->setUser($user)
			->run();

		$this->assertEquals(count($result), 5);
		foreach ($result as $user)
		{
			$this->assertInstanceOf(User::class, $user);
		}
	}

	public function testInTheStartOfList()
	{
		$user = User::findOne(1, self::$dbh);
		$result = MatchMaker::create(self::$dbh)
			->setUser($user)
			->run();

		$this->assertEquals(count($result), 5);
		foreach ($result as $user)
		{
			$this->assertInstanceOf(User::class, $user);
		}
	}

	public function testInTheEndOfList()
	{
		$user = User::findOne(99, self::$dbh);
		$result = MatchMaker::create(self::$dbh)
			->setUser($user)
			->run();

		$this->assertEquals(count($result), 5);
	}

	public function testFiltering()
	{
		$user = User::findOne(50, self::$dbh);
		$result = MatchMaker::create(self::$dbh)
			->setUser($user)
			->setFilter(range(40, 60))
			->run();
		$this->assertEquals(count($result), 0);

		$user = User::findOne(50, self::$dbh);
		$result = MatchMaker::create(self::$dbh)
			->setUser($user)
			->setFilter(range(41, 60))
			->run();
		$this->assertEquals(count($result), 1);
		$this->assertEquals($result[0]->id, 40);
	}
}
