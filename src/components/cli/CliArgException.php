<?php namespace components\cli;

/**
 * Class CliArgException
 * @package components\cli
 */
class CliArgException extends \Exception {}