<?php namespace components\cli;

/**
 * Class CliArg
 * Retrieve and validate CLI argument
 * @package components
 */
class CliArg
{
	/** @var int */
	private int $index;
	/** @var bool */
	private bool $required = false;
	/** @var callable|null */
	private $filter = null;
	/** @var mixed */
	private $default = null;

	/**
	 * @param int $index
	 * @return CliArg
	 * @see __construct()
	 */
	public static function create(int $index): CliArg
	{
		return new self($index);
	}

	/**
	 * CliArg constructor.
	 * @param int $index Index in $argv
	 */
	public function __construct(int $index)
	{
		$this->index = $index;
	}

	/**
	 * Is argument required
	 * @param bool $required
	 * @return $this
	 */
	public function setRequired(bool $required): CliArg
	{
		$this->required = $required;
		return $this;
	}

	/**
	 * Filter function for adjust value format.
	 * Function format:
	 * ```php
	 *  function ($value) { return $value; }
	 * ```
	 * @param callable $filter
	 * @return $this
	 */
	public function setFilter(callable $filter): CliArg
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * Default value if argument is not presented
	 * @param mixed $default
	 * @return $this
	 */
	public function setDefault($default): CliArg
	{
		$this->default = $default;
		return $this;
	}

	/**
	 * @return mixed
	 * @throws CliArgException
	 */
	public function get()
	{
		global $argv;

		if ($this->required && !isset($argv[$this->index])) {
			throw new CliArgException('Argument required');
		}

		if (!isset($argv[$this->index])) {
			return $this->default;
		}

		$value = $argv[$this->index];
		if (is_callable($this->filter)) {
			$value = call_user_func($this->filter, $value);
		}

		return $value;
	}
}