<?php namespace components\db;

use PDO;

/**
 * Class PDOConfigurator
 * Configure PDO DB handler by the config with default params
 * @package components
 */
class PDOConfigurator
{
	private const DEFAULTS = [
		'dsn'             => 'pgsql:host=localhost;dbname=',
		'username'        => '',
		'password'        => '',
		'charset'         => 'UTF-8',
		'throwOnError'    => true,
		'emulatePrepares' => true,
	];

	/**
	 * @param array $config
	 * @see DEFAULTS
	 * @return PDO
	 */
	public static function configure(array $config): PDO
	{
		$config = array_merge(self::DEFAULTS, $config);
		$dbh = new PDO($config['dsn'], $config['username'], $config['password']);

		$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, (bool) $config['emulatePrepares']);
		if ($config['throwOnError']) {
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}

		$stmt = $dbh->prepare('SET NAMES :charset;');
		$stmt->execute([':charset' => $config['charset']]);

		return $dbh;
	}
}
