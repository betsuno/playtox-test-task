<?php namespace components\game;

use components\exceptions\InvalidCallException;
use Exception;
use models\User;
use PDO;
use PDOStatement;

/**
 * Class MatchMaker
 * Finding opponents for player
 * @package components
 */
class MatchMaker
{
	/** @var int Size of sample shoulder */
	const DELTA = 10;
	/** @var int Size of resulting batch */
	const RESULT_BATCH_SIZE = 5;

	private PDO $dbh;
	/** @var User|null */
	private ?User $user = null;
	/** @var int[] */
	private array $filter = [0];

	public static function create(PDO $dbh): MatchMaker
	{
		return new self($dbh);
	}

	public function __construct(PDO $dbh)
	{
		$this->dbh = $dbh;
	}

	/**
	 * @param User $user Player to search
	 * @return $this
	 */
	public function setUser(User $user): MatchMaker
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * List of player user ids that should be excluded from result
	 * @param array $filter
	 * @return $this
	 */
	public function setFilter(array $filter): MatchMaker
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * Run search
	 * @return array
	 * @throws InvalidCallException
	 * @throws Exception
	 * @return User[]
	 */
	public function run(): array
	{
		$this->validate(true);

		$stmt = $this->createStatement();
		$stmt->bindValue(':delta', self::DELTA, PDO::PARAM_INT);
		$stmt->bindValue(':userId', $this->user->id, PDO::PARAM_INT);
		$stmt->bindValue(':authority', $this->user->authority, PDO::PARAM_INT);
		$stmt->execute();

		$result = [];
		/** @var User $user */
		while ($user = $stmt->fetchObject(User::class)) {
			if (in_array($user->id, $this->filter)) {
				continue;
			}
			$result[] = $user;
		}
		return self::getRandomBatch($result);
	}

	/**
	 * Validate parameters
	 * @param bool $throw
	 * @return bool
	 * @throws InvalidCallException
	 */
	public function validate(bool $throw = false): bool
	{
		if (!$this->user instanceof User) {
			if (!$throw) {
				return false;
			}
			throw new InvalidCallException('User must be provided');
		}

		if ($this->filter === null) {
			return true;
		}

		foreach ($this->filter as $item)
		{
			if (!is_int($item)) {
				if (!$throw) {
					return false;
				}
				throw new InvalidCallException('Values of filter must be integers');
			}
		}

		return true;
	}

	private function createStatement(): PDOStatement
	{
		return $this->dbh->prepare(<<<SQL
			WITH before AS (
				SELECT *
				FROM users
				WHERE
				  ((id < :userId AND authority = :authority)
				  OR authority < :authority)
				ORDER BY authority DESC, id DESC
				LIMIT :delta * 2
			),
			after AS (
				SELECT *
				FROM users
				WHERE
				  ((id > :userId AND authority = :authority)
				  OR authority > :authority)
				  AND id NOT IN (SELECT id FROM before)
				ORDER BY authority, id
				LIMIT :delta * 2 - least(:delta, (SELECT count(*) FROM before))
			),
			result AS (
				(
					SELECT *
					FROM before
					ORDER BY authority DESC, id DESC
					LIMIT :delta * 2 - (SELECT count(*) FROM after)
				) UNION (
					SELECT *
					FROM after
					ORDER BY authority, id
				)
			)
			SELECT *
			FROM result
			ORDER BY authority, id
			SQL
		);
	}

	/**
	 * Returns randomised batch restricted by RESULT_BATCH_SIZE
	 * @param User[] $users
	 * @return User[]
	 * @see RESULT_BATCH_SIZE
	 */
	private static function getRandomBatch(array $users): array
	{
		$result = [];
		if (!count($users)) {
			return [];
		}
		$batchSize = min(count($users), self::RESULT_BATCH_SIZE);
		foreach ((array) array_rand($users, $batchSize) as $index)
		{
			$result[] = $users[$index];
		}
		return $result;
	}
}
