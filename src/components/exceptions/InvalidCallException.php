<?php namespace components\exceptions;

/**
 * Class InvalidCallException
 * @package components\exceptions
 */
class InvalidCallException extends \Exception {}