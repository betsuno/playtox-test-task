<?php namespace models;

use PDO;

/**
 * Class User
 * Represents users table records
 * @package models
 */
class User
{
	public int $id;
	public string $username;
	public int $authority;

	/**
	 * @param int $id User ID
	 * @param PDO $dbh
	 * @return User|null
	 */
	public static function findOne(int $id, PDO $dbh): ?User
	{
		$stmt = $dbh->prepare('SELECT * FROM users WHERE id = :id');
		$stmt->execute([':id' => $id]);
		$result = $stmt->fetchObject(self::class);
		return $result !== false ? $result : null;
	}
}
